//
//  MEMenuSplitRootControllerExample.m
//  MEUniversalMenu
//
//  Created by denn on 11/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuSplitRootControllerExample.h"

@interface MEMenuSplitRootControllerExample () <MEMenuConfigureProtocol>

@end

@implementation MEMenuSplitRootControllerExample

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.configureDelegate = self;
        self.menuItemsSegueID = @"menuSplitRootSegueID";
    }
    
    return self;
}

- (UIButton*) nextButton{
    UIButton  * back_button =[[UIButton alloc] init];
    [back_button setTitle:[NSString stringWithFormat:@"Right>"] forState:UIControlStateNormal];
    back_button.frame = CGRectMake(0, 0, 60, 29);
    back_button.backgroundColor = [UIColor clearColor];
    back_button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    [back_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    return back_button;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    UIView *background = [[UIView alloc] initWithFrame:self.view.frame];
//    background.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:background];
//    [background.superview sendSubviewToBack:background];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
