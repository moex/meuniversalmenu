//
//  MEMainMenuContentController.h
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEUniversalMenu.h"

@interface MEMainMenuItemsControllerExample : MEMenuItemsController <MEMenuItemsSourceProtocol>
@end
