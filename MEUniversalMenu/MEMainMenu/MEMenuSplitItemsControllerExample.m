//
//  MERightMenuContentController.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 02.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuSplitItemsControllerExample.h"

@interface MEMenuSplitItemsControllerExample () <MEMenuItemsSourceProtocol>

@end

@implementation MEMenuSplitItemsControllerExample
{
    MEMenuModel *menuModel;
}


- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    
    return self;
}

- (MEMenuItem*) initialMenuItem{
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    MEMenuItem *item = [self.menuContent menuForIndexPath: index];
    item.initialHidden = YES;
    item.hasDisclosureIndicator = YES;
    return item;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    
    self.tableView.delegate = self;
	// Do any additional setup after loading the view.
    
    
    menuModel = [[MEMenuModel alloc] init];
    
    MEMenuSection *_section = [menuModel addSectionWithCaption:@""];
            
    [_section.items addObject:[MEMenuItem
                               menuItemWithID:@"rightMenuContent"
                               withCaption:NSLocalizedString(@"Menu 1", @"Menu item ")
                               withIcon: [UIImage imageNamed:@"menu_icon_watchlist"]
                               withSelectedIcon: nil
                               forViewController:@"rightMenuDetailsController"]];
    
    [_section.items addObject:[MEMenuItem
                               menuItemWithID:@"rightMenuContent"
                               withCaption:NSLocalizedString(@"Menu 2", @"Menu item News")
                               withIcon:[UIImage imageNamed:@"menu_icon_watchlist"]
                               withSelectedIcon: nil
                               forViewController:@"rightMenuDetailsController"]];

    self.menuContent = menuModel;
    
    self.itemsSource = self;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
