//
//  MEMainMenuContent.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMainMenuContentExample.h"

static MEMainMenuContentExample *__shared_instance = nil;

@implementation MEMainMenuContentExample
- (void) configure{
    //
    // Markets menu
    //
    MEMenuSection *_section = [_menu addSectionWithCaption:@""];

    MEMenuItem *login = [MEMenuItem
                         menuItemWithID:@"login"
                         withCaption:@"John Smith"
                         withIcon:[UIImage imageNamed:@"menu_icon_change_user"]
                         withSelectedIcon: nil
                         forViewController:@"loginNavigationControllerID"];
    
    login.hasDisclosureIndicator = NO;
    [_section.items addObject:login];

    _section = [_menu addSectionWithCaption:NSLocalizedString(@"Markets", @"Menu Markets Section")];
    
    [_section.items addObject:[MEMenuItem
                               menuItemWithID:@"watchList"
                               withCaption:NSLocalizedString(@"Watch List", @"Menu item Watchlist")
                               withIcon:[UIImage imageNamed:@"menu_icon_watchlist"]
                               withSelectedIcon: [UIImage imageNamed:@"menu_icon_watchlist_selected"]
                               forViewController:@"watchListNavigationControllerID"]];
    
    [_section.items addObject:[MEMenuItem
                               menuItemWithID:@"settings"
                               withCaption:NSLocalizedString(@"Settings", @"Menu item Settings")
                               withIcon:[UIImage imageNamed:@"menu_icon_settings"]
                               withSelectedIcon: [UIImage imageNamed:@"menu_icon_settings_selected"]
                               forViewController:@"settingsNavigationControllerID"]];
}


+ (id) sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_instance = [[MEMainMenuContentExample alloc] init];
    });
    
    return __shared_instance;
}

- (id) init{
    
    @synchronized (self) {
        
        //        if (!__shared_instance) {
        //            return __shared_instance;
        //        }
        
        self = [super init];
        
        _menu = [[MEMenuModel alloc] init];
        
        [self configure];
        
        __shared_instance = self;
    }
    
    return self;
}


@end
