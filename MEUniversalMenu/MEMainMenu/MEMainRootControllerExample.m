//
//  MEMainItemsViewController.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 02.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMainRootControllerExample.h"

@interface MEMainRootControllerExample () <MEMenuConfigureProtocol>

@end

@implementation MEMainRootControllerExample

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.menuItemsSegueID = @"mainMenuItems";
        self.configureDelegate = self;
    }
    
    return self;
}

- (UIButton*) backButton:(MEMenuItem *)menuItem{
    
    UIButton  * back_button =[[UIButton alloc] init];
    [back_button setTitle:[NSString stringWithFormat:@"<%@",menuItem.menuID] forState:UIControlStateNormal];
    back_button.frame = CGRectMake(0, 0, 60, 29);
    back_button.backgroundColor = [UIColor clearColor];
    back_button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    [back_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    return back_button;
}


- (MEMenuItemsSide) menuViewSide{
    return ME_MENU_ITEMS_LEFT;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
