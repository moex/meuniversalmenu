//
//  MEMainMenuContentController.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMainMenuItemsControllerExample.h"
#import "MEMainMenuContentExample.h"
#import "MEMenuPopoverConfigureProtocol.h"

@interface MEMainMenuItemsControllerExample () <MEMenuPopoverConfigureProtocol>

@end

@implementation MEMainMenuItemsControllerExample

- (UIButton*) popoverViewDoneButton{
    
    UIButton  * back_button =[[UIButton alloc] init];
    [back_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"Close", @"")] forState:UIControlStateNormal];
    back_button.frame = CGRectMake(0, 0, 60, 29);
    back_button.backgroundColor = [UIColor clearColor];
    back_button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    [back_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    return back_button;
}


- (CGSize) popoverViewSize{
    return CGSizeMake(400, 400);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.tableView.delegate = self;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.menuContent = [[MEMainMenuContentExample sharedInstance] menu];
    self.itemsSource = self;
    self.popoverConfigure = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Menu view data source

- (UITableViewCell*) menuItem:(MEMenuItem *)menuItem cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *text =  (UILabel*)[cell viewWithTag:1];
    
    text.text = menuItem.caption;
    
    UIImageView *icon =  (UIImageView*)[cell viewWithTag:2];

    icon.image = menuItem.icon;
    icon.highlightedImage = menuItem.selectedIcon;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *_cell = [tableView cellForRowAtIndexPath:indexPath];
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor greenColor]];
    [_cell setSelectedBackgroundView:bgColorView];
    
}

- (MEMenuItem*) initialMenuItem{
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:1];
    MEMenuItem *item = [self.menuContent menuForIndexPath: index];
    item.initialHidden = NO;
    return item;
}

@end
