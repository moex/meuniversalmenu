//
//  MEMainMenuContent.h
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEUniversalMenu.h"

@interface MEMainMenuContentExample : NSObject
@property (readonly,nonatomic) MEMenuModel *menu;
+ (id) sharedInstance;
@end
