//
//  MEMenuSplitContentPushSegue.h
//  MEUniversalMenu
//
//  Created by denn on 04.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Segue push item controller to view screen after menu itme was selected.
 */
@interface MEMenuSplitPushSegue : UIStoryboardSegue
@end
