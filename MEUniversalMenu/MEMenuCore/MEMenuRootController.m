//
//  MEViewController.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuRootController.h"
#import "MEMenuItemsController.h"

@interface MEMenuRootController () <UIGestureRecognizerDelegate>
@property (nonatomic,strong) UINavigationController* selectedContent;
@property (nonatomic,strong) UIView *menuPanelView;
@property (nonatomic,strong) UIView *slideView;
@property (nonatomic,strong) UIView *outSlideView;
@end

@implementation MEMenuRootController{
    CGRect           originMenuFrame;
    MEMenuItemsSide  menuSide;
    
    NSMutableDictionary *navigationBarCache;
}

- (void) configure{
    self.menuRootWidth = MEUI_MENU_WIDTH;
    self.gestureMinSlideVelocity = MEUI_MENU_GESTURE_MINSLIDE_VELOCITY;
    self.menuGestureEdges = MEUI_MENU_GESTURE_EDGES;
    self.menuItemsSegueID = MEUI_MENU_ITEMS_SEGUE;
    
    navigationBarCache = [[NSMutableDictionary alloc] init];
}

- (id) init{
    self = [super init];
    if (self) {
        [self configure];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return  self;
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self configure];
    }
    return self;
}

- (void) addOutSlideView{
    
    if (!_outSlideView) {
        CGFloat x = _slideView.frame.origin.x+_slideView.frame.size.width;
        CGFloat width = self.view.frame.size.height; // it needs while cheking landscape orientation
        _outSlideView = [[UIView alloc] initWithFrame: CGRectMake(x, 0, width, self.view.frame.size.height)];
        _outSlideView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tapOutslide = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutSlide:)];
        [_outSlideView addGestureRecognizer:tapOutslide];
    }
    
    [self.view addSubview:_outSlideView];
}

- (void) tapOutSlide: (UITapGestureRecognizer*)gesture{
    [self hideMenuViewAnimated:YES completition: nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
        
    if (self.configureDelegate && [self.configureDelegate respondsToSelector:@selector(menuViewSide)]) 
        menuSide = [self.configureDelegate menuViewSide];
    else
        menuSide = ME_MENU_ITEMS_LEFT;
    
    CGRect bounds = self.view.bounds;
        
    if (menuSide == ME_MENU_ITEMS_RIGHT) {
        bounds.origin.x =  bounds.size.width-_menuRootWidth;
    }
    
    // main menu
    
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        bounds.size.height = bounds.size.width;
        bounds.size.width = self.menuRootWidth;
    }
    else
        bounds.size.width = self.menuRootWidth;
    
    _slideView = [[UIView alloc] initWithFrame:bounds];
    _slideView.backgroundColor = self.view.backgroundColor;
        
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        bounds.origin.y = 20;
        bounds.size.height -= 20;
    }
    
    _menuPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, bounds.origin.y, _slideView.frame.size.width,  bounds.size.height)];
    _menuPanelView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:_slideView];
    [_slideView addSubview:_menuPanelView];
    
    originMenuFrame = _slideView.frame;
    
    [self performSegueWithIdentifier:self.menuItemsSegueID sender:self];
    
    UIPanGestureRecognizer *pangesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHanlder:)];
    pangesture.maximumNumberOfTouches = 2;
    pangesture.delegate = self;
    [self.view addGestureRecognizer:pangesture];
    
}

- (void) panHanlder: (UIPanGestureRecognizer*)gesture{
    UIView* panningView = gesture.view;
    CGPoint translation = [gesture translationInView:panningView];
    UIView* movingView = _slideView;

    CGFloat velocity = [gesture velocityInView:movingView].x;
    CGFloat originx  = movingView.frame.origin.x;

    if (_slideView.alpha<1.0) {
        _slideView.alpha = 1.0;
        [_slideView.superview bringSubviewToFront:_slideView];
    }
    
    
    if ([gesture state] == UIGestureRecognizerStateEnded){
        
        if (menuSide == ME_MENU_ITEMS_LEFT){
            if (velocity > self.gestureMinSlideVelocity) {                
                
                [self showMenuViewAnimated:YES completition:nil];
                
            } else if (velocity < -self.gestureMinSlideVelocity) {                
                
                [self hideMenuViewAnimated:YES completition: nil];
                
            } else if (originx > -originMenuFrame.size.width / 2.0f) {
                
                [self showMenuViewAnimated:YES completition:nil];

            } else {
                
                [self hideMenuViewAnimated:YES completition: nil];
            }
        }
        else{
            if (velocity > self.gestureMinSlideVelocity) {                
                
                [self hideMenuViewAnimated:YES completition: nil];
                
            } else if (velocity < -self.gestureMinSlideVelocity) {
                
                [self showMenuViewAnimated:YES completition:nil];
                
            } else if (originx > originMenuFrame.size.width / 2.0f) {
                
                [self hideMenuViewAnimated:YES completition: nil];
                
            } else {
                [self showMenuViewAnimated:YES completition:nil];
            }
        }        
    } else {
        
        if (menuSide == ME_MENU_ITEMS_LEFT) {
            if (originx + translation.x > 0 ) 
                translation.x = 0.0;
            else if (originx + translation.x < - originMenuFrame.size.width) 
                translation.x= 0.0;            
        }
        else{
            if (originx + translation.x <  self.view.frame.size.width-self.menuRootWidth) 
                translation.x = 0.0;
        }                
        
        movingView.center = CGPointMake(movingView.center.x + translation.x, movingView.center.y);
        
        [gesture setTranslation:CGPointZero inView:[panningView superview]];
    }
}

- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint touchPosition = [gestureRecognizer locationInView:self.view];
    
    BOOL touch_inside = YES;
        
    if (menuSide == ME_MENU_ITEMS_LEFT) {
        if (_slideView.frame.origin.x+_slideView.frame.size.width>0) 
            //
            // Menu is opened
            //
            touch_inside = touchPosition.x<=_slideView.frame.size.width;        
        else
            //
            // Menu is hidden && touch is inside edges
            //
            touch_inside = (touchPosition.x < self.menuGestureEdges);        
    }
    else{
        if (_slideView.frame.origin.x<self.view.frame.size.width) 
            //
            // Menu is opened && touch inside in _slideView
            //
            touch_inside = touchPosition.x>=_slideView.frame.origin.x; 
        else
            //
            // Menu is hidden && touch inside in edges
            //
            touch_inside = (touchPosition.x >= (self.view.frame.size.width-self.menuGestureEdges));        
    }
    
    if (!touch_inside)
        [self hideMenuViewAnimated:YES completition: nil];
    
    return touch_inside;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void) __setMenuController{
    CGRect bounds = _menuPanelView.bounds;
    
    _menuItemsController.view.frame = bounds;
            
    CALayer* layer = _slideView.layer;
    layer.shadowColor = [UIColor grayColor].CGColor;
    layer.masksToBounds = NO;
    layer.shadowPath =[UIBezierPath bezierPathWithRect:layer.bounds].CGPath;

    if (self.configureDelegate && [self.configureDelegate respondsToSelector:@selector(menuViewLayer:)]) {
        [self.configureDelegate performSelector:@selector(menuViewLayer:) withObject:_slideView];
    }
    else{
        layer.shadowOpacity = MEUI_MENU_SHADOW_OPACITY;
        layer.shadowOffset = MEUI_MENU_SHADOW_OFFSET;
        layer.shadowRadius = MEUI_MENU_SHADOW_RADIUS;
    }
}

- (void) setMenuItemsController:(MEMenuItemsController *)menuController{
    if (_menuItemsController && _menuItemsController != menuController) {
        [_menuItemsController willMoveToParentViewController:nil];
        [_menuItemsController removeFromParentViewController];
        [_menuItemsController.view.superview removeFromSuperview];
    }

    _menuItemsController = menuController;

    [self __setMenuController];
    
    [_menuItemsController willMoveToParentViewController:self];
    [self addChildViewController:menuController];
    [_menuPanelView addSubview:menuController.view];
    
    if (_menuItemsController.itemsSource && [_menuItemsController.itemsSource performSelector:@selector(initialMenuItem)]) {
        MEMenuItem *item = [_menuItemsController.itemsSource performSelector:@selector(initialMenuItem)];
        _menuItemsController.selectedMenuItem = item;
        if (item){
            [self pushMenuItemController:item hideMenu: NO];
            if (item.initialHidden){
                [self hideMenu];
                _slideView.alpha = .0;
            }
            else
                [self addOutSlideView];
        }
    }
    
    [_menuItemsController didMoveToParentViewController:self];
}


- (void) hideMenu{
    _slideView.frame = CGRectMake( (menuSide == ME_MENU_ITEMS_LEFT?-originMenuFrame.size.width:self.view.frame.size.width), originMenuFrame.origin.y, originMenuFrame.size.width, originMenuFrame.size.height);
}

- (void) hideMenuViewAnimated:(BOOL)animated completition:(void (^)(BOOL finished))completion{
    [UIView
     animateWithDuration: animated?MEUI_MENU_ANIMATION_DELAY:0
     animations:^{
                  
         [self hideMenu];
     }
     completion:^(BOOL finished){         
         [_outSlideView removeFromSuperview];
         
         [UIView animateWithDuration:MEUI_MENU_ANIMATION_DELAY delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            _slideView.alpha = 0.0;
         } completion:nil];                  

         if (completion) {
             completion(finished);
         }
         
         if (_menuItemsController.itemsSource && [_menuItemsController.itemsSource respondsToSelector:@selector(menu:onState:)]) {
             MEMenuItem *menuItem = _menuItemsController.selectedMenuItem;
             [_menuItemsController.itemsSource menu:menuItem onState:NO];
         }
     }];
}

- (void) showMenuViewAnimated:(BOOL)animated completition:(void (^)(BOOL finished))completion{
    [_slideView.superview bringSubviewToFront:_slideView];
    _slideView.alpha = 1.0;
    [UIView
     animateWithDuration: animated?MEUI_MENU_ANIMATION_DELAY:0
     animations:^{
         CGRect bounds = CGRectMake( originMenuFrame.origin.x, originMenuFrame.origin.y, originMenuFrame.size.width, originMenuFrame.size.height);
         _slideView.frame = bounds;
     }
     completion:^(BOOL finished){
         if (completion) {
             completion(finished);
         }
         
         [self addOutSlideView];
         
         if (_menuItemsController.itemsSource && [_menuItemsController.itemsSource respondsToSelector:@selector(menu:onState:)]) {
             MEMenuItem *menuItem = _menuItemsController.selectedMenuItem;
             [_menuItemsController.itemsSource menu:menuItem onState:YES];
         }

     }];
}

- (void) show{
    [self showMenuViewAnimated:YES completition:nil];
}

- (void) backButtonHandler{
    [self showMenuViewAnimated:YES completition:nil];
}

- (void) setSelectedContent:(UINavigationController *)content{
    
    _selectedContent = content;

    CGRect bounds = self.view.bounds;
    
    content.view.frame = CGRectMake(0,0,bounds.size.width,bounds.size.height);
    
    [self addChildViewController:content];
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
    
    if (content.visibleViewController == [content.viewControllers firstObject]) {
        UIButton *back_button;
        
        UINavigationItem *navigationItem = self.selectedContent.navigationBar.topItem;
        UIBarButtonItem  *buttonItem;
        MEMenuItem *menuItem = self.menuItemsController.selectedMenuItem;
        
        if (self.configureDelegate && [self.configureDelegate respondsToSelector:@selector(backButton:)]){
            back_button = [self.configureDelegate performSelector:@selector(backButton:) withObject:menuItem];
            [back_button addTarget:self action:@selector(backButtonHandler) forControlEvents:UIControlEventTouchUpInside];
            buttonItem = [[UIBarButtonItem alloc] initWithCustomView:back_button];
        }
        else
            buttonItem = [[UIBarButtonItem alloc]
                          initWithTitle:menuItem.caption
                          style: UIBarButtonItemStylePlain
                          target:self
                          action:@selector(backButtonHandler)
                          ];
        
        if (menuSide == ME_MENU_ITEMS_LEFT)
            navigationItem.leftBarButtonItem = buttonItem;
        else
            navigationItem.rightBarButtonItem = buttonItem;
    }
}

- (void) __swicthContentController:(UINavigationController *)content{
    
    if (!content)
        return;
    
    self.view.userInteractionEnabled = NO;
    
    UIViewController *previuos_controller, *new_selected_controller = content;
    
    previuos_controller = _selectedContent;
    new_selected_controller = content;
    
    [new_selected_controller viewWillAppear:YES];
    [previuos_controller viewWillDisappear:YES];
    
    [UIView animateWithDuration:MEUI_ANIMATION_DELAY/2. 
                          delay:0.0 
                        options:0 
                     animations:^{
                         self.selectedContent.view.alpha = 0.5;
                     } 
                     completion:^(BOOL finished){
                         [previuos_controller viewDidDisappear:YES];
                     }
      ];
    
    if (_selectedContent) {
        [self.selectedContent willMoveToParentViewController:nil];
        [self.selectedContent.view removeFromSuperview];
        [self.selectedContent removeFromParentViewController];
    }
    
    content.view.alpha = 0.5;
    
    self.selectedContent = content;
    
    [UIView animateWithDuration:MEUI_ANIMATION_DELAY/2.
                          delay:0.0
                        options:0
                     animations:^{
                         self.selectedContent.view.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         [new_selected_controller viewDidAppear:YES];
                     }
     ];
    
    self.view.userInteractionEnabled = YES;
}

- (void) pushMenuItemController:(MEMenuItem*)menuItem hideMenu:(BOOL)hide{
    UINavigationController *content = menuItem.controller;
    
    
    content.navigationBar.topItem.title = menuItem.caption;
    
    if (!_selectedContent) {
        self.selectedContent = content;
        [_slideView.superview bringSubviewToFront:_slideView];
        return;
    }
    
    if (_selectedContent && _selectedContent == content) {
        if (hide) [self hideMenuViewAnimated:YES completition: nil];
        return;
    }

    
    if (hide) [self hideMenuViewAnimated: YES completition: ^(BOOL finished){
        [self __swicthContentController:content];
    }];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];

    CGFloat height = 0.0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        height = 20.0;
    }

    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        _menuItemsController.view.frame = CGRectMake(0, _menuItemsController.view.frame.origin.y, self.menuRootWidth, self.view.frame.size.width-height);
    }
    else{
        _menuItemsController.view.frame = CGRectMake(0, _menuItemsController.view.frame.origin.y, self.menuRootWidth, self.view.frame.size.height-height);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
