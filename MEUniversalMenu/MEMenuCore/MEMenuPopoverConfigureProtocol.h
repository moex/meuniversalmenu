//
//  MEMenuPopoverConfigureProtocol.h
//  MEUniversalMenu
//
//  Created by denis svinarchuk on 15.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  This protocol defines how popover view presents on screen.
 */
@protocol MEMenuPopoverConfigureProtocol <NSObject>

/**
 *  Configure the popover menu navigation bar "done/close" button.
 *
 *  @return UIButton object.
 */
- (UIButton*) popoverViewDoneButton;

/**
 *  Configure the popover view size.
 *  
 *  @return size.
 */
- (CGSize) popoverViewSize;

@end
