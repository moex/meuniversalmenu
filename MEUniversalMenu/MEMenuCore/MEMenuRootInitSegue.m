//
//  MERootSegue.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuRootInitSegue.h"
#import "MEMenuItemsController.h"

@implementation MEMenuRootInitSegue

- (void) perform{
    MEMenuRootController *rootController = self.sourceViewController;
    rootController.menuItemsController = self.destinationViewController;
    rootController.menuItemsController.rootController = rootController;
}

@end
