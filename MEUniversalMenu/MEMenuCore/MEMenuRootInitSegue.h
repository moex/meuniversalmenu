//
//  MERootSegue.h
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEMenuRootController.h"
#import "MEMenuItemsController.h"

/**
 *  Initialize Root menu.
 */
@interface MEMenuRootInitSegue : UIStoryboardSegue
@end
