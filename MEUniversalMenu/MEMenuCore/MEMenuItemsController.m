//
//  MEMenuContentController.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuItemsController.h"

@interface MEMenuItemsController ()
@property(nonatomic,strong) UIPopoverController *currentPopoverController;
@end

@implementation MEMenuItemsController

@synthesize menuContent=_menuContent;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (self.itemsSource && [self.itemsSource
                                 respondsToSelector:@selector(initialMenuItem)]) {
        
        MEMenuItem *initialMenu = [self.itemsSource performSelector:@selector(initialMenuItem)];
        _selectedMenuItem = initialMenu;
        for (int i=0; i < self.menuContent.sections.count; i++) {
            NSArray *rows = [self.menuContent.sections[i] items];
            for (int r=0; r < rows.count; r++) {
                MEMenuItem *menuItem = rows[r];
                if ([menuItem.menuID isEqualToString:initialMenu.menuID]) {
                    NSIndexPath *path = [NSIndexPath indexPathForRow:r inSection:i];
                    [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
                    //[self tableView:self.tableView didHighlightRowAtIndexPath:path];
                    return;
                }
            }
        }
    }
}

- (void)viewDidLoad
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // self.tableView.bounces = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setMenuContent:(MEMenuModel *)menuContent{
    _menuContent = menuContent;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (MEMenuModel*) menuContent{
    return _menuContent;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.menuContent.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.menuContent.sections[section] items] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MEMenuItem *menuItem = [self.menuContent menuForIndexPath:indexPath];

    UITableViewCell *cell;
    if (self.itemsSource && [self.itemsSource respondsToSelector:@selector(menuItem:cellForRowAtIndexPath:)]) {
        cell = [self.itemsSource performSelector:@selector(menuItem:cellForRowAtIndexPath:) withObject:menuItem withObject:indexPath];
        if (!menuItem.hasDisclosureIndicator) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else{
    
        static NSString *CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        UILabel *text =  (UILabel*)[cell viewWithTag:1];
        
        text.text = menuItem.caption;
        
    }
    return cell;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.menuContent.sections[section] caption];
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    _selectedMenuItem = [self.menuContent menuForIndexPath:indexPath];
//    [self performSegueWithIdentifier:self.selectedMenuItem.menuID sender:self];
//}


- (NSIndexPath*) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedMenuItem = [self.menuContent menuForIndexPath:indexPath];
    
    if (self.itemsSource && [self.itemsSource respondsToSelector:@selector(menu:willSelectMenuIndexPath:)]) {
        [self.itemsSource menu:_selectedMenuItem willSelectMenuIndexPath:indexPath];
    }
    
    [self performSegueWithIdentifier:self.selectedMenuItem.menuID sender:self];
    
    return indexPath;
}

#pragma mark - Navigation

- (void) doneButtonHandler{
    NSLog(@"Done");
    [self.currentPopoverController dismissPopoverAnimated:YES];
}


// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]]) {
        self.currentPopoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
        _popoverRect = [NSValue valueWithCGRect:[self.tableView rectForRowAtIndexPath:[self.tableView indexPathForSelectedRow]]];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}


@end
