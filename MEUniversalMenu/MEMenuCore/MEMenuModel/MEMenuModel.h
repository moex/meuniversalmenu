//
//  MEMenuModel.h
//  MExchangeInfo-Universal
//
//  Created by denn on 27.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEMenuItem.h"

/**
 *  *Menu section.*
 *  Any menus can comprise numbers groups of item. One group defines one section.
 */
@interface MEMenuSection : NSObject;
/**
 *  Localized section name.
 */
@property(nonatomic,readonly) NSString *caption;

/**
 *  Menu items belong the section.
 */
@property(nonatomic) NSMutableArray *items;
@end


/**

 *Menu model.*
 
 Sometimes, application contains many views contain sufficient amount of information. In this case, modern mobile devices have some limitations to show all data within one screen or view.
 Such a way we have to split to represent our information in different way in variouse screean.
 
 The main MEUMenu idea is to split navigation through application and manage views which implements application logic.
 So, a menu is a collection of views that can be shown on the device screen while we need to switch ViewController context.
 I.e.: Menu representation is a ViewController needs to switch between different ViewControllers. That's all. 
 
 Of cause one app can contain a lot of menus, or a menu hierarchy. In this case, you just add new splited level menu. 
 
 */
@interface MEMenuModel : NSObject

/**
 *  Menu sections.
 */
@property(nonatomic) NSMutableArray *sections;

/**
 *  Add news section.
 *
 *  @param aSectionCaption Localized section name.
 *
 *  @return menu section instance.
 */
- (MEMenuSection*) addSectionWithCaption: (NSString*)aSectionCaption;

/**
 *  Get Menu item for index path.
 *
 *  @param path index path.
 *
 *  @return menu item.
 */
- (MEMenuItem*) menuForIndexPath: (NSIndexPath*)path;
@end
