//
//  MEMenuItem.m
//  MExchangeInfo-Universal
//
//  Created by denn on 27.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuItem.h"

@interface MEMenuItem()
@property(nonatomic) NSString *menuID;
@property(nonatomic) NSString *caption;
@property(nonatomic) UIImage  *icon;
@property(nonatomic) UIImage  *selectedIcon;
@property(nonatomic) UINavigationController *controller;
@end

@implementation MEMenuItem
+ (id) menuItemWithID:(NSString*) aMenuID withCaption:(NSString*)aCaption withIcon: (UIImage*)anIcon withSelectedIcon:(UIImage *)anSIcon forViewController:(id)aController{
    
    MEMenuItem *_menu = [[MEMenuItem alloc] init];
    
    _menu.menuID = aMenuID;
    _menu.caption = aCaption;
    _menu.icon = anIcon;
    
    if (!anSIcon) {
        _menu.selectedIcon = _menu.icon;
    }
    else
        _menu.selectedIcon = anSIcon;
    
    if ([aController isKindOfClass:[NSString class]]) {
        UINavigationController *_rc =  (UINavigationController*)[[UIApplication sharedApplication] delegate].window.rootViewController;
        _menu.controller = [[_rc storyboard] instantiateViewControllerWithIdentifier: (NSString*)aController];
    }
    else
        _menu.controller = (UINavigationController*) aController;

    _menu.hasDisclosureIndicator = YES;
    
    return _menu;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@ [%@/%@] %@", _menuID, _caption, _icon, _selectedIcon, _controller];
}

@end
