//
//  MEMenuItem.h
//  MExchangeInfo-Universal
//
//  Created by denn on 27.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  *Menu item*.
 *  To create menu sliding views in apps using MEUMenu you should define menu structure. Menu structure is defined by menu items collection.
 *  Collection can be created statically once, when application starts, or dinamically every time you need to show new views contains menu.
 *
 */
@interface MEMenuItem : NSObject

/**
 *  Prepare a menu item.
 *
 *  @param aMenuID     menu ID string.
 *  @param aCaption    localized caption.
 *  @param anIcon      menu icon, can be nil if you don't need to view specific menu icon.
 *  @param anSIcon     menu selected icon, can be nil if we don't need highlighting selection.
 *  @param aController controller object or string references to Storyboard controller ID. Controller should inherit from NSNavigation controller.
 *
 *  @return menu item instance object.
 */
+ (id) menuItemWithID:(NSString*) aMenuID withCaption:(NSString*)aCaption withIcon: (UIImage*)anIcon withSelectedIcon: (UIImage*)anSIcon forViewController:(id)aController;

/**
 *  Menu item uniq ID. Uniq is in context of whole app. It uses to create segue relations between view controller on storyboard.
 */
@property(readonly, nonatomic) NSString *menuID;

/**
 *  Localized menu caption.
 */
@property(readonly, nonatomic) NSString *caption;

/**
 *  Icon image.
 */
@property(readonly, nonatomic) UIImage  *icon;

/**
 *  Selected icon image.
 */
@property(readonly, nonatomic) UIImage  *selectedIcon;

/**
 *  Controller coonected with the menu item.
 */
@property(readonly, nonatomic) UINavigationController *controller;

/**
 *  Yes if menu items has disclosure icon.
 */
@property(nonatomic) BOOL hasDisclosureIndicator;

/**
 *  YES if you need to hide menu at the start.
 */
@property(nonatomic) BOOL initialHidden;

@end
