//
//  MEMenuModel.m
//  MExchangeInfo-Universal
//
//  Created by denn on 27.10.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuModel.h"

@interface MEMenuSection() 
@property(nonatomic) NSString *caption;
@end

@implementation MEMenuSection

- (id) init{
    self = [super init];
    
    if (self) {
        _items = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (NSString*) description{
    NSMutableString *s = [NSMutableString new];
    [s appendFormat:@"%@\n", _caption];
    for (MEMenuItem *item in _items) {
        [s appendFormat:@"    %@", item];
    }
    return s;
}

@end

@implementation MEMenuModel

- (id)init{
    self = [super init];
    
    if (self) {
        _sections = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (MEMenuItem*) menuForIndexPath:(NSIndexPath *)path{
    MEMenuSection *_section = [_sections objectAtIndex:path.section];
    return _section.items[path.row];
}

- (MEMenuSection*) addSectionWithCaption: (NSString*)aSectionCaption{
    
    MEMenuSection *_section = [[MEMenuSection alloc] init];
    _section.caption = aSectionCaption;
    
    [_sections addObject:_section];
    
    return _section;
}


- (NSString*) description{
    NSMutableString *s = [NSMutableString new];
    for (MEMenuSection *section in _sections) {
        [s appendFormat:@"%@", section];
    }
    return s;
}


@end
