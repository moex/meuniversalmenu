//
//  MEMenuItemsPopoverSegue.m
//  MEUniversalMenu
//
//  Created by denis svinarchuk on 14.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuItemsPopoverSegue.h"
#import "MEMenuItemsController.h"
#import "MEMenuRootController.h"
#import "MEMenuPopoverConfigureProtocol.h"

@interface MEMenuItemsPopoverSegue()
@end

@implementation MEMenuItemsPopoverSegue

- (id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination{
    self = [super initWithIdentifier:identifier source:source destination:destination];    
    return self;
}

- (void) perform{
    
    UIPopoverController *popoverC = self.popoverController;
    
    MEMenuItemsController    *menuContentController = self.sourceViewController;
    UIViewController         *destination = self.destinationViewController;    
        
    if (popoverC && [menuContentController respondsToSelector:@selector(popoverRect)]) {
    
        if ([destination isKindOfClass:[UINavigationController class]]) {
            UINavigationController *controller = (UINavigationController*) destination;
            
            controller.navigationBar.topItem.title = menuContentController.selectedMenuItem.caption;
            
            if (menuContentController.popoverConfigure && [menuContentController.popoverConfigure respondsToSelector:@selector(popoverViewDoneButton)]) {
                UIButton *doneButton = [menuContentController.popoverConfigure performSelector:@selector(popoverViewDoneButton)];
                [doneButton addTarget:menuContentController action:@selector(doneButtonHandler) forControlEvents:UIControlEventTouchUpInside];        
                controller.navigationBar.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
            }            
        }   

        popoverC.delegate = menuContentController;
        
        if (menuContentController.popoverConfigure && [menuContentController.popoverConfigure respondsToSelector:@selector(popoverViewSize)]) {
            popoverC.popoverContentSize = [menuContentController.popoverConfigure popoverViewSize];
        }
        else 
            popoverC.popoverContentSize = CGSizeMake(320, 640);                
        
        [popoverC 
         presentPopoverFromRect:[[menuContentController performSelector:@selector(popoverRect)] CGRectValue] 
         inView: menuContentController.view
         permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES
         ];
    }
    
}

@end
