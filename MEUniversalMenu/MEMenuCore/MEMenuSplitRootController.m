//
//  MEMenuSplitViewController.m
//  MEUniversalMenu
//
//  Created by denn on 03.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuSplitRootController.h"
#import "MEMenuDefaults.h"

@interface MEMenuSplitRootController () <UIGestureRecognizerDelegate>
@property (nonatomic,strong) UIViewController* selectedContentController;
@property (nonatomic,strong) NSString* menuItemsTitle;
@property (nonatomic,strong) UIViewController *rootViewController;
@property (nonatomic,strong) UIView *outSlideView;
@end

@implementation MEMenuSplitRootController
{
    UIBarButtonItem  *rightButtonItem;
    UIBarButtonItem  *leftButtonItem;
    
    UIPanGestureRecognizer *panGesture;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self configure];
    }
    return self;
}

- (void) configure{
    self.menuItemsSegueID = MEUI_MENU_SPLIT_ITEMS_SEGUE;
    self.menuRootWidth = MEUI_MENU_WIDTH;
    self.gestureMinSlideVelocity = MEUI_MENU_GESTURE_MINSLIDE_VELOCITY;
    self.menuGestureEdges = MEUI_MENU_GESTURE_EDGES;
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return self;
}

- (void) addOutSlideView{
    
    if (!_outSlideView) {
        CGFloat x = 0;
        CGFloat width = MAX(self.view.bounds.size.width, self.view.bounds.size.height); // it needs while cheking landscape orientation
        _outSlideView = [[UIView alloc] initWithFrame: CGRectMake(x, 0, width, width)];
        _outSlideView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tapOutslide = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutSlide:)];
        [_outSlideView addGestureRecognizer:tapOutslide];
    }
    [self.view insertSubview:_outSlideView belowSubview:_menuItemsNavigationController.view];
}

- (void) tapOutSlide: (UITapGestureRecognizer*)gesture{
    [self slideTo:UINavigationControllerOperationPop completion:nil];
}

- (UIViewController *) rootViewController{
   return  self.navigationController.topViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _menuItemsTitle = self.menuItemsNavigationController.navigationBar.topItem.title;
    
	// Do any additional setup after loading the view.
    
    [self performSegueWithIdentifier:self.menuItemsSegueID sender:self];
    
    
    
    if (self.configureDelegate && [self.configureDelegate respondsToSelector:@selector(nextButton)]){
        UIButton *back_button = [self.configureDelegate performSelector:@selector(nextButton)];
        [back_button addTarget:self action:@selector(nextButtonHandler) forControlEvents:UIControlEventTouchUpInside];
        rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:back_button];
    }
    else
        rightButtonItem = [[UIBarButtonItem alloc]
                           initWithTitle:NSLocalizedString(@"Split Menu", @"Default Split menu title")
                           style: UIBarButtonItemStylePlain
                           target:self
                           action:@selector(nextButtonHandler)
                           ];
    
    leftButtonItem = [[UIBarButtonItem alloc]
                         initWithTitle:self.menuItemsNavigationController.navigationBar.topItem.title
                         style: UIBarButtonItemStylePlain
                         target:self
                         action:@selector(backLeftButtonHandler)
                         ];
    
    self.navigationBar.topItem.rightBarButtonItem = rightButtonItem;
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
//        self.navigationBar.frame = CGRectMake(self.navigationBar.frame.origin.x, 20.0f, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height+20);
//    }
    
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandler:)];
    panGesture.maximumNumberOfTouches = 2;
    panGesture.delegate = self;
    [self.view addGestureRecognizer:panGesture];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void) addToNavigationController{
    if (![self findTheController:self.menuItemsNavigationController]) {
        
        [self.view addSubview: _menuItemsNavigationController.view];
        
        [_menuItemsNavigationController willMoveToParentViewController:self];
        [self addChildViewController: _menuItemsNavigationController];
        [_menuItemsNavigationController didMoveToParentViewController:self];
    }
}

- (void) removeFromNavigationController{
    [_menuItemsNavigationController willMoveToParentViewController:self];
    [_menuItemsNavigationController removeFromParentViewController];
    [_menuItemsNavigationController.view removeFromSuperview];
}

- (void) panHandler: (UIPanGestureRecognizer*) gesture{
    UIView* panningView = gesture.view;
    CGPoint translation = [gesture translationInView:panningView];
    UIView* movingView = _menuItemsNavigationController.view;
    
    CGFloat velocity = [gesture velocityInView:movingView].x;
    CGFloat originx  = movingView.frame.origin.x;
        
    [self addToNavigationController];
    
    if ([gesture state] == UIGestureRecognizerStateEnded){
        
        if (velocity > self.gestureMinSlideVelocity)
            
            [self slideTo:UINavigationControllerOperationPop completion:nil];
        
        else if (velocity < -self.gestureMinSlideVelocity)
            
            [self slideTo:UINavigationControllerOperationPush completion:nil];
        
        else if ((self.view.frame.size.width-originx) <=  self.menuRootWidth/2.0f + self.menuGestureEdges)
            
            [self slideTo:UINavigationControllerOperationPop completion:nil];
        
        else
            [self slideTo:UINavigationControllerOperationPush completion:nil];
        
    } else {
        
        if (originx + translation.x <  self.view.frame.size.width-self.menuRootWidth)
            translation.x = 0.0;
        else if (movingView.frame.origin.x>=self.view.frame.size.width && velocity>0)
            //
            // sliding at edge
            //
            translation.x = 0;
        
        movingView.center = CGPointMake(movingView.center.x + translation.x, movingView.center.y);
        
        [gesture setTranslation:CGPointZero inView:[panningView superview]];
    }
}

- (void) slideTo:(UINavigationControllerOperation) operation completion:(void (^)(BOOL finished))completion{
    UIView* movingView = _menuItemsNavigationController.view;

    CGFloat centerX = self.view.bounds.size.width+_menuItemsNavigationController.view.bounds.size.width/2.;
    CGFloat rootCenterX = self.view.bounds.size.width/2.;

    if (operation == UINavigationControllerOperationPush) {
        centerX = self.view.bounds.size.width+_menuItemsNavigationController.view.bounds.size.width/2.-self.menuRootWidth;
        rootCenterX = self.view.bounds.size.width/2.-self.menuRootWidth;
    }
    else{
        centerX = self.view.bounds.size.width+_menuItemsNavigationController.view.bounds.size.width/2.;
        rootCenterX = self.view.bounds.size.width/2.;
    }
    
    if (UINavigationControllerOperationPush)
        [self addOutSlideView];

    [UIView
     animateWithDuration:MEUI_MENU_ANIMATION_DELAY
     delay:0
     options:0
     animations:^{
         movingView.center = CGPointMake(centerX, movingView.center.y);
     }
     completion:^(BOOL finished){
         if (completion) {
             completion(finished);
         }
         if (operation == UINavigationControllerOperationPop) {
             [self removeFromNavigationController];
             [_outSlideView removeFromSuperview];
         }
     }];
}


- (BOOL) gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint touchPosition = [gestureRecognizer locationInView:self.view];
    UIView* movingView = _menuItemsNavigationController.view;
    
    BOOL touch_inside = YES;
    
    if (movingView.frame.origin.x<self.view.frame.size.width)
        //
        // Menu is opened && touch inside in _slideView
        //
        touch_inside = touchPosition.x>=movingView.frame.origin.x || touchPosition.x<=self.menuGestureEdges;
    else
        //
        // Menu is hidden && touch inside in edges
        //
        touch_inside = (touchPosition.x >= (self.view.frame.size.width-self.menuGestureEdges));
        
    return touch_inside;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (UIViewController*) findTheController: (UIViewController*)controller{
    for (UIViewController *c in self.childViewControllers) {
        //
        if (controller == c) {
            return c;
        }
    }
    return nil;
}

- (void) setMenuControllerSize{
    CGRect content_bounds = self.view.bounds;
    _menuItemsNavigationController.view.frame = CGRectMake(content_bounds.size.width, _menuItemsNavigationController.view.frame.origin.y, self.menuRootWidth, content_bounds.size.height);
}

- (void) setMenuItemsNavigationController:(UINavigationController *)menuItemsNavigationController{
    
    _menuItemsNavigationController =  menuItemsNavigationController;

    //
    // menu content frame
    //

    [self setMenuControllerSize];
    
    CALayer* layer = _menuItemsNavigationController.view.layer;
    layer.shadowColor = [UIColor grayColor].CGColor;
    layer.masksToBounds = NO;
    layer.shadowPath =[UIBezierPath bezierPathWithRect:layer.bounds].CGPath;
    
    if (self.configureDelegate && [self.configureDelegate respondsToSelector:@selector(menuViewLayer:)]) {
        [self.configureDelegate performSelector:@selector(menuViewLayer:) withObject:_menuItemsNavigationController.view];
    }
    else{
        layer.shadowOpacity = MEUI_MENU_SHADOW_OPACITY;
        layer.shadowOffset = MEUI_MENU_SHADOW_OFFSET;
        layer.shadowRadius = MEUI_MENU_SHADOW_RADIUS;
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void) popMenuItemViewController{
    
    UIViewController *fromVC  =  self.menuItemsNavigationController;
    UIView           *containerView = self.view;
    UINavigationController *navigationController = self.menuItemsNavigationController;

    [UIView
     animateWithDuration:MEUI_MENU_ANIMATION_DELAY
     delay:0
     options:0
     animations:^{         
         fromVC.view.frame = CGRectMake( containerView.frame.size.width - self.menuRootWidth, containerView.frame.origin.y, self.menuRootWidth, containerView.frame.size.height);
     }
     completion:^(BOOL finished){
         panGesture.enabled = YES;
         [_selectedContentController willMoveToParentViewController:navigationController];
         [_selectedContentController removeFromParentViewController];
         [_selectedContentController.view removeFromSuperview];
     }];
    
}

- (void) pushMenuItemController:(MEMenuItem *)menuItem{
    
    panGesture.enabled = NO;

    UINavigationBar *navigationBar = self.menuItemsNavigationController.navigationBar;
    UINavigationController *navigationController = self.menuItemsNavigationController;
    
    UIViewController *toVC = menuItem.controller;

    _menuItemsTitle = navigationController.navigationBar.topItem.title;
    _selectedContentController = toVC;
    
    [toVC willMoveToParentViewController:navigationController];
    [navigationController addChildViewController:toVC];
    [navigationController.view addSubview:toVC.view];
    
    toVC.view.frame = CGRectMake(navigationBar.frame.origin.x, navigationBar.frame.origin.y+navigationBar.frame.size.height, toVC.view.frame.size.width, navigationController.view.frame.size.height-(navigationBar.frame.origin.y+navigationBar.frame.size.height));

    navigationController.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, navigationController.view.frame.size.height);
    toVC.view.frame = CGRectMake(toVC.view.frame.origin.x, toVC.view.frame.origin.y, navigationController.view.frame.size.width, toVC.view.frame.size.height);
    
    navigationController.navigationBar.topItem.leftBarButtonItem = leftButtonItem;
    navigationController.navigationBar.topItem.title = menuItem.caption;
    
    [UIView beginAnimations:nil context: (void*) nil];
    {
        [UIView setAnimationDelegate:self];
        
        [UIView setAnimationDuration: MEUI_ANIMATION_DELAY];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:navigationController.view cache:NO];
    }
    [UIView commitAnimations];
    
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context{
    [_selectedContentController didMoveToParentViewController:self.menuItemsNavigationController];
}

- (void) backLeftButtonHandler{

    UINavigationBar *navigationBar = self.menuItemsNavigationController.navigationBar;
    UINavigationController *navigationController = self.menuItemsNavigationController;
    
    [_selectedContentController willMoveToParentViewController:nil];
    [_selectedContentController.view removeFromSuperview];
    [_selectedContentController removeFromParentViewController];
    
    navigationBar.topItem.leftBarButtonItem = nil;
    navigationBar.topItem.title = _menuItemsTitle;
    
    [self popMenuItemViewController];
    [UIView beginAnimations:nil context: (void*) nil];
    {
        [UIView setAnimationDuration: MEUI_ANIMATION_DELAY];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:navigationController.view cache:NO];
        navigationController.view.frame = CGRectMake(self.view.frame.size.width-self.menuRootWidth, navigationController.view.frame.origin.y, self.menuRootWidth, navigationController.view.frame.size.height);
    
    }
    [UIView commitAnimations];

    panGesture.enabled = YES;
}

- (void) slideInOut:(void (^)(BOOL finished))completion{
    
    UINavigationControllerOperation operation;
    
    if (self.rootViewController.view.frame.origin.x>=0)
        operation = UINavigationControllerOperationPush;
    
    else
        operation = UINavigationControllerOperationPop;
    
    [self slideTo:operation  completion:completion];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
        
    UIApplication *app = [UIApplication sharedApplication];
    CGFloat height = self.navigationBar.bounds.size.height + MIN(app.statusBarFrame.size.height, app.statusBarFrame.size.width);
    if (_selectedContentController != _menuItemsNavigationController.topViewController) {
        [self slideTo:UINavigationControllerOperationPop completion:nil];
        [self setMenuControllerSize];
    }
    else{
        _selectedContentController.view.frame=CGRectMake(0, height, self.view.bounds.size.width, self.view.bounds.size.height-height);
    }
}

- (void) nextButtonHandler{
    [self addToNavigationController];
    [self slideInOut:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
