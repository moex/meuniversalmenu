//
//  MEMenuItemsConfigureProtocol.h
//  MEUniversalMenu
//
//  Created by denn on 03.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEMenuModel.h"

/**
 *  Menu items side definitions.
 */
typedef enum {
    ME_MENU_ITEMS_RIGHT = 1000,
    ME_MENU_ITEMS_LEFT
} MEMenuItemsSide;


/**
 *  Configure Menu items view
 */
@protocol MEMenuConfigureProtocol <NSObject>
@optional

/**
 *  Configure menu navigation bar back button.
 *
 *  @param menuItem menu item definition object.
 *
 *  @return UIButton object.
 */
- (UIButton*) backButton: (MEMenuItem*)menuItem;

/**
 *  Configure menu navigation bar next splited menu button.
 *
 *  @return UIButton object.
 */
- (UIButton*) nextButton;

/**
 *  It makes menu view configurable;
 *
 *  @param view UIView object reference.
 *
 *  @return view layer.
 */
- (CALayer*) menuViewLayer: (UIView*)view;

/**
 *  Where menu should be located
 *
 *  @return items side definition. Can be: ME_MENU_ITEMS_RIGHT or ME_MENU_ITEMS_LEFT.
 *  @warning *Warning: The current version supports for root menu controller only. Splited menus are always right.*
 */
- (MEMenuItemsSide) menuViewSide;

@end

