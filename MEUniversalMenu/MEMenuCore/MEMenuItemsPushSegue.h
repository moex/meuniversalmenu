//
//  MEContentMenuSegue.h
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Segue push item controller to view screen after menu itme was selected.
 */
@interface MEMenuItemsPushSegue : UIStoryboardSegue
@end
