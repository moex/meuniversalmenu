//
//  MEMenuSplitToMenuSegue.h
//  MEUniversalMenu
//
//  Created by denn on 03.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Initialize Split Menu Segue. 
 */
@interface MEMenuSplitRootInitSegue : UIStoryboardSegue
@end
