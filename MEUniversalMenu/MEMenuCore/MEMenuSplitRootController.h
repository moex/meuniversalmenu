//
//  MEMenuSplitViewController.h
//  MEUniversalMenu
//
//  Created by denn on 03.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEMenuConfigureProtocol.h"
#import "MEMenuItemsController.h"

/**
  This is the nex level menu root controller. It splits representation main root menu logic from other menu dinamicaly use within the app which creates dinamicaly on demands.
You **should** use **one** MEMenuRootController and **many** MEMenuSplitRootControllers per one app.
 */
@interface MEMenuSplitRootController : UINavigationController

/**
 * This segue ID should be defined to initialize *topViewController* of the next level application logic. This one is the custom controller view that can have additional options
 * to view something differents in separated view controllers. In this case you add Splited menu controller to switch this level representaion context.
 */
@property (nonatomic) NSString *detailViewSegueID;

/**
 *  Menu items for splited menu.
 */
@property (nonatomic) NSString *menuItemsSegueID;

/**
 *  NavigationController attached menu items.
 */
@property (nonatomic,strong) UINavigationController *menuItemsNavigationController;

/**
 *  Where slide gestrure is sensetive, edges size in pixel. Default is MEUI_MENU_GESTURE_EDGESpx.
 */
@property (nonatomic) CGFloat menuGestureEdges;

/**
 *  Minimal slide velocity to hide/open menu. Default is MEUI_MENU_GESTURE_MINSLIDE_VELOCITY.
 */
@property (nonatomic) CGFloat gestureMinSlideVelocity;

/**
 *  Width main menu items view. Default is MEUI_MENU_WIDTH.
 */
@property (nonatomic) CGFloat menuRootWidth;


/**
 *  Delegate configuration menu items view.
 */
@property (nonatomic) id<MEMenuConfigureProtocol> configureDelegate;

/**
 *  Show view item controller and push menu items view.
 *
 *  @param menuItem menu item.
 */
- (void) pushMenuItemController:(MEMenuItem*)menuItem;
@end
