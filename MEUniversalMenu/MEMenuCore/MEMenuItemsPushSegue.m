//
//  MEContentMenuSegue.m
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuItemsPushSegue.h"
#import "MEMenuItemsController.h"
#import "MEMenuRootController.h"

@implementation MEMenuItemsPushSegue

- (void)perform{
    MEMenuItemsController    *menuContentController = self.sourceViewController;
    MEMenuRootController     *rootMenuController = (MEMenuRootController*) menuContentController.rootController;
    [rootMenuController pushMenuItemController:menuContentController.selectedMenuItem hideMenu:YES];
}

@end
