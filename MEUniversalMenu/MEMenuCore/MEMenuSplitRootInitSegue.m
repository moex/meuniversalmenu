//
//  MEMenuSplitToMenuSegue.m
//  MEUniversalMenu
//
//  Created by denn on 03.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuSplitRootInitSegue.h"
#import "MEMenuSplitRootController.h"

@implementation MEMenuSplitRootInitSegue

- (void) perform{
    MEMenuSplitRootController *splitView = self.sourceViewController;
    UINavigationController *menuItemsNavigationController = self.destinationViewController;
    MEMenuItemsController *menuItemsController = (MEMenuItemsController*) menuItemsNavigationController.topViewController;
    
    splitView.menuItemsNavigationController = menuItemsNavigationController;
    menuItemsController.rootController = splitView;
}

@end
