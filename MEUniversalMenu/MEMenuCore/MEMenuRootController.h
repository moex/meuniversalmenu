//
//  MEViewController.h
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEMenuItemsController.h"
#import "MEMenuDefaults.h"
#import "MEMenuConfigureProtocol.h"


/**
 * *MEMenuRootController* manages the root application menu. Root means that is one and main menu which this app pattern uses.
 */
@interface MEMenuRootController : UINavigationController

/**
 *  This is a menu items content view controller. It controls context switching between others view controller, which can define its own custom logic and behavior.
 *  Every menu item links to the self-defined viewcontroller. Links can be cross-references. The controller *have to* segued from root controller.
 */
@property (nonatomic) MEMenuItemsController *menuItemsController;

/**
 *  This is a menu items initial Segue identifier string. Reference to top ViewController defined by menuItemsController
 *  @warning You **should** define this property before initialize object instance or wthin your version of initXXX. Default ID is @"menuRootSegueID".
*   @see menuItemsController.
 */
@property (nonatomic) NSString *menuItemsSegueID;

/**
 *  Where slide gestrure is sensetive, edges size in pixel. Default is MEUI_MENU_GESTURE_EDGESpx.
 */
@property (nonatomic) CGFloat menuGestureEdges;

/**
 *  Minimal slide velocity to hide/open menu. Default is MEUI_MENU_GESTURE_MINSLIDE_VELOCITY.
 */
@property (nonatomic) CGFloat gestureMinSlideVelocity;

/**
 *  Width main menu items view. Default is MEUI_MENU_WIDTH.
 */
@property (nonatomic) CGFloat menuRootWidth;

/**
 *  Delegate configuration menu items view.
 */
@property (nonatomic) id<MEMenuConfigureProtocol> configureDelegate;

/**
 *  Show view item controller and push menu items view.
 *
 *  @param menuItem menu item.
 *  @param hide     YES if menu view would be hidden after selection stage.
 */
- (void) pushMenuItemController:(MEMenuItem*)menuItem hideMenu:(BOOL)hide;

/**
 *
 */
- (void) show;

@end
