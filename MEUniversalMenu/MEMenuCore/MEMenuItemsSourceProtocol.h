//
//  MEMenuItemsSourceProtocol.h
//  MEUniversalMenu
//
//  Created by denn on 11/5/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEMenuModel.h"

/**
 *  Menu items view controller protocol services delegation to view menu items cell. Usually menu represents as a UITableViewController inheritor.
 */
@protocol MEMenuItemsSourceProtocol <NSObject>
/**
 * To show from initial menu item controller view.
 */
- (MEMenuItem*) initialMenuItem;

@optional

/**
 *  Render menu item cell.
 *  Use @"Cell" to define UITableViewCell in storyboard, tag 1 for MenuItem:caption, tag 2 for MenuItem:icon to avoid overload the delegate often.
 *  @param menuItem  menu item definition.
 *  @param indexPath item place.
 *
 *  @return ViewCell.
 */
- (UITableViewCell*) menuItem:(MEMenuItem*)menuItem cellForRowAtIndexPath:(NSIndexPath*)indexPath;

/**
 *  State menu
 *
 *  @param item         menu item
 *  @param hidden_shown state is hide or shown NO/YES
 */
- (void) menu:(MEMenuItem*)item onState:(BOOL)hidden_shown;

/**
 *  Select menuitem
 *
 *  @param item      menu item
 *  @param indexPath index path
 */
- (void) menu:(MEMenuItem *)item willSelectMenuIndexPath:(NSIndexPath*)indexPath;

@end