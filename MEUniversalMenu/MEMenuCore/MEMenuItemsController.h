//
//  MEMenuContentController.h
//  MEExchangeInfo-Universal-Menu
//
//  Created by denn on 10/30/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEMenuItemsSourceProtocol.h"
#import "MEMenuPopoverConfigureProtocol.h"

/**
 *  Controller manages selected view content.
 */
@interface MEMenuItemsController : UITableViewController <UIPopoverControllerDelegate>

/**
 *  Get selected menu item.
 */
@property(nonatomic,weak) MEMenuItem  *selectedMenuItem;

/**
 *  Menu content structure views within the items controller.
 */
@property(strong) MEMenuModel *menuContent;

/**
 *  Delegate source of menu items.
 */
@property(nonatomic,strong) id<MEMenuItemsSourceProtocol> itemsSource;

/**
 *  Delegate popover view configuration.
 */
@property(nonatomic,strong) id<MEMenuPopoverConfigureProtocol> popoverConfigure;

/**
 *  Reference to root menu items view controller, ie the app MEMenuRootController instance.
 */
@property(nonatomic,strong) UINavigationController *rootController;

@property(nonatomic,strong) NSValue *popoverRect;

- (void) doneButtonHandler;

@end
