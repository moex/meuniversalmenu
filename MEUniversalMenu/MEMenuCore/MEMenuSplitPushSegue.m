//
//  MEMenuSplitContentPushSegue.m
//  MEUniversalMenu
//
//  Created by denn on 04.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "MEMenuSplitPushSegue.h"
#import "MEMenuItemsController.h"
#import "MEMenuSplitRootController.h"


@implementation MEMenuSplitPushSegue


- (void)perform{
    MEMenuItemsController      *menuContentController = self.sourceViewController;
    MEMenuSplitRootController  *rootController = (MEMenuSplitRootController*) menuContentController.rootController;
    [rootController pushMenuItemController:menuContentController.selectedMenuItem];
}

@end
