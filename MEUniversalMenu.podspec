Pod::Spec.new do |s|

  s.name         = "MEUniversalMenu"
  s.version      = "1.0.0"
  s.summary      = "This a very simple next iOS Menu viewControllers extention"

  s.description  = <<-DESC
                   This is a very simple next iOS Menu viewControllers extention. 
                   As opposed to many "slide menus" iOS libs, like SaSlideMenu, it uses "tree" relations between controller  concept. 
                   It seems more complicated than flat approach, but gives more flexible programm code control. For instance, you can create various
                   second level menu in the right side comprise different controllers, not only one managed view controller such as in SaSlide.
                   
                   MEUniversalMenu can be used for iPad project, unlike "slides menu", because does not move any navigation controller, only view controllers which contain menu items. Unfortunately this is "classical" behavior does not use in availible menu extentions. So, menu moves over controllers and this idea drammaticaly simplifies developing one.                                                                                                                                                                       
                                                        
                   DESC

  s.homepage     = "https://bitbucket.org/denn_nevera/meuniversalmenu-me-projects-ios"

  s.license      = { :type => 'GPL Version 3', :file => 'LICENSE' }

  s.author       = { "denn nevera (book pro)" => "denn.nevera@gmail.com" }

  s.platform     = :ios, '7.0'

  s.source       = { :git => "https://denn_nevera@bitbucket.org/denn_nevera/meuniversalmenu-me-projects-ios.git", :tag => "1.0.0" }

  s.source_files  = 'MEUniversalMenu/MEMenuCore/*.{h,m}', 'MEUniversalMenu/MEMenuCore/**/*.{h,m}'

  s.public_header_files = 'MEUniversalMenu/MEMenuCore/*.h', 'MEUniversalMenu/MEMenuCore/**/*.{h}'

  s.resources    = 'MEUniversalMenu/MEMenuDocs'

  s.frameworks = 'UIKit', 'Foundation', 'CoreGraphics'

  s.requires_arc = true

end
