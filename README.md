# MEUniversalMenu

Created by **denn.nevera**

This is a very simple next iOS Menu viewControllers extention.  As opposed to many "slide menus" iOS libs, like SaSlideMenu, it uses "tree" relations between controller  concept. It seems more complicated than flat approach, but gives more flexible programm code control. For instance, you can create various second level menu in the right side comprise different controllers, not only one managed view controller such as in SaSlide.
                   
MEUniversalMenu can be used for iPad project, unlike "slides menu", because does not move any navigation controller, only view controllers which contain menu items. Unfortunately this is "classical" behavior does not use in availible menu extentions. So, menu moves over controllers and this idea drammaticaly simplifies developing one.                                                                                                                                                                       

The project code is used as a part of Moscow Exchange mobile projects.

# A main reason to create another cicle

* Free
* No one open source project, which i could find, gives hierarchical menu representation
* No one does it under iOS7
* Menu is a model
* UITables are always dynamic cell
* Relations and behavior should be described in StroryBoard
* iPhone/iPad
* I need a little bit confused menu structure in our projects, i need a simple code to realize it:)

# How to use

* Install cocoapods: http://cocoapods.org/
* Edit Podfile in the your XCode project directory 
YourApp:  
    $ edit Podfile        
    platform :ios, '7.0'
    pod 'MEUniversalMenu', :git => 'https://denn_nevera@bitbucket.org/denn_nevera/meuniversalmenu-me-projects-ios.git', :branch => 'master'            
            
* Install dependences in your 
project 
    $ pod install

* Open the XCode workspace instead of the project 
file
    $ open YourApp.xcworkspace

* Import 
API
    \#import "MEUniversalMenu.h"




